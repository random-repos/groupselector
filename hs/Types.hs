module Types
(
    Student, Game(..), Group(..), History,
    getGroupGame, getGroupStudents, getGameCreator, groupContainsStudent, isInstructor,
    showGroup,
    nullStudent, nullGame
) where

import Data.List

type Student = String
type Game = (String,Student,(Integer,Integer)) --game name, student, player range
data Group = Group [Student] Game deriving (Eq, Show)
type History = [Group]

--instance Show Group where
--show :: Group -> String
--show (Group stds game) = show game ++ show stds
showGroup :: Group -> String
showGroup (Group stds (game,creator,_)) = 
	game ++ " : " ++ creator ++ "\n" ++
	intercalate "\n" (fmap (++"  ") stds)

nullStudent = ""
nullGame = ("",nullStudent,(-1,-1))
nullGroup = Group [] nullGame

getGroupGame (Group _ g) = g
getGroupStudents (Group s _) = s
getGameCreator (_,std,(_,_)) = std
groupContainsStudent (Group stds _) std = [std] `isInfixOf` stds
isInstructor std = "*" `isInfixOf` std
