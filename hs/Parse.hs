module Parse
(
    parseSectionByTitle,
    parseSection,
    parseStudents,
    parseGames,
    parseHistory
)
where

import Types
import Debug.Trace (trace)
import Data.Char (isSpace)
import qualified Data.Map (lookup, fromList)
import qualified Data.List (find)
import qualified Data.Maybe (isJust)

trimSpaces :: String -> String
trimSpaces = f . f
    where f = reverse . dropWhile isSpace

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =
    case dropWhile p s of
        "" -> []
        s' -> w : wordsWhen p s''
            where (w, s'') = break p s'

isInstructor :: Student -> Bool
isInstructor std = Data.Maybe.isJust $ Data.List.find (\c->c=='*') std

parseSectionByTitle :: String -> String -> [String]
parseSectionByTitle title fileContents = parseSectionByTitle' title $ lines fileContents

parseSectionByTitle' :: String -> [String] -> [String]
parseSectionByTitle' title fileContents
    | lnCnt == 0 = trace "Error, file contents empty" [] --TODO throw an exception here, too hard, I think it needs to be IO class :(
    | ln !! 0 == "::"++title = parseSection lnTail []
    | otherwise = parseSectionByTitle' title lnTail
    where
        ln = fileContents
        lnTail = tail ln
        lnCnt = length ln

parseSection :: [String] -> [String] -> [String]
parseSection ln output
    | lnCnt == 0 = output
    | take 2 firstLn == "::" = output
    | trimSpaces firstLn == "" || 
        (trimSpaces firstLn) !! 0 == '#' = parseSection lnTail $ output
    | otherwise = parseSection lnTail $ output ++ [firstLn]
    where
        lnCnt = length ln
        firstLn = ln !! 0
        lnTail = tail ln

parseStudents :: String -> [Student]
parseStudents a = parseSectionByTitle "STUDENTS" a

parseGames :: String -> [Game]
parseGames a = fmap (parseGame.parseAuthor) lst
    where
        parseAuthor x = fmap trimSpaces $ wordsWhen (==':') x --String->[String]
        lst = parseSectionByTitle "GAMES" a --[String]
        parseGame :: [String] -> Game
        parseGame (x:y:z:a:[]) = (x,y,(read z, read a)) --[String]->Game
        parseGame (x:y:z:[]) = (x,y,(read z, read z)) --[String]->Game
        parseGame _ = trace "Error, bad formatting in games" nullGame

parseHistory :: String -> [Game] -> History
parseHistory a games = fmap (parseGroup.parseAuthor) lst
    where
        parseAuthor x = fmap trimSpaces $ wordsWhen (==':') x --String->[String]
        lst = parseSectionByTitle "HISTORY" a --[String]
        parseGroup :: [String] -> Group
        parseGroup (x:y) = Group y game
            where
                Just game = Data.List.find (\(a,_,_) -> a == x ) games
        parseGroup _ = trace "Error, bad formatting in history" $ Group [] nullGame
