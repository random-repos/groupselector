import System.Random
import System.Environment
import Data.List
import Data.List.Split
import System.IO
import Debug.Trace
import Data.Monoid
import Data.Maybe
import System.Random.Shuffle (shuffle')

--my stuff
import Types
import Parse
import SetOperations



generateRandomGroup :: StdGen -> [Student] -> [Game] -> [Group]
generateRandomGroup rng stds games =
    let
        sstds = shuffle' stds (length stds) rng
        rng2 = (snd $ next rng)
        sgms = shuffle' games (length games) (snd $ next rng) 
        rng3 = (snd $ next rng2)
        gamePlayers = fmap (\(_,_,(pRange)) -> fromIntegral $ fst $ randomR pRange rng3) sgms
        stdGrps = partitionUntilEmpty sstds gamePlayers
    in
        zipWith (\lstds lgame -> Group lstds lgame) stdGrps $ take (length stdGrps) sgms

instructors :: [Student] -> [Student]
instructors stds = filter isInstructor stds

numTimesGamePlayedByStudent :: Game -> History -> Student -> Int
numTimesGamePlayedByStudent g h s =
    let
        subGroups = filter (\(Group stds game) -> game == g) h
        subsubGroups = filter (\(Group stds _) -> isInfixOf [s] stds) subGroups
    in
        length subsubGroups

numGamesPlayedByStudent :: History -> Student -> Int
numGamesPlayedByStudent h s = length $ fmap (\(Group stds _) -> [s] `isInfixOf` stds) h


groupHeuristic :: [Student] -> History -> Group -> Int
groupHeuristic stds hist g =
    let
        game = getGroupGame g
        creator = getGameCreator game
        groupStds = getGroupStudents g
        instr = instructors stds
        numInstr = length instr
        totalGamesPlayed = length hist
        numTimesGamePlayed = length $ filter (\(Group _ x)->x == game) hist
        hasBeenPlayedByInstructor = or $ fmap (/=0) $ fmap (numTimesGamePlayedByStudent game hist) (instr)
        multiplePlays = foldr max 0 $ map (\x -> numTimesGamePlayedByStudent game hist x) groupStds 
        studentNotInHistory = length $ filter (\x -> numGamesPlayedByStudent hist x == 0) groupStds
        numInstrInGroup = length (groupStds `intersect` instr)
        creatorInGroup = if hasBeenPlayedByInstructor == False && creator /= nullStudent
            then
                if  groupContainsStudent g creator && 
                    numInstrInGroup == 1
                then 200 else 0
            else 0
        --this is a bad idea, needs to be based on ratio of games instructor was in or something like that
        --instrAdjust = if hasBeenPlayedByInstructor && numInstrInGroup > 0 then -50 else 0
        instrAdjust = 0
    in
        --DELETE redundant to numTimesGamePlayed
        --numStudentsPlayedGame*(-1) + 
        numTimesGamePlayed*(-1) +
        --studentNotInHistory*(10) + 
        creatorInGroup + 
        multiplePlays*(-10) +
        instrAdjust

netGroupHeuristic :: [Student] -> History -> [Group] -> Int
netGroupHeuristic stds hist grps 
    | grps == [] = -100000
    | otherwise = sum $ fmap (groupHeuristic stds hist) grps

--takes students, group, and history and returns next set of groups
finalizeGroups :: StdGen -> [Student] -> [Game] -> History -> [Group]
finalizeGroups rng stds gms hist = 	
    let
        foldingFunc :: Int -> ([Group],StdGen) -> ([Group],StdGen)
        foldingFunc item acc =
            let
                lhs = netGroupHeuristic stds hist (fst acc)
                newRng = snd $ next $ snd acc
                newGroups = generateRandomGroup newRng stds gms
                rhs = netGroupHeuristic stds hist newGroups
            in
                --if trace (show newGroups ++ show lhs ++ " " ++ show rhs) $ lhs > rhs then (fst acc, newRng) else (newGroups,newRng)
                if lhs > rhs then (fst acc, newRng) else (newGroups,newRng)
    in
        fst $ foldr foldingFunc ([],rng) [1..5000]

newFileContents :: [Student] -> [Game] -> History -> String
newFileContents students games hist =
    let
        studentsString = intercalate "\n" students
        gameToString (g,s,(mn,mx)) = intercalate " : " [g,s,show mn, show mx]
        gamesString = intercalate "\n" $ fmap gameToString games
        groupToString (Group stds (g,_,_)) = g ++ " : " ++ (intercalate " : " stds)
        histString = intercalate "\n" $ fmap groupToString hist
    in
        "::STUDENTS\n" ++ studentsString ++
        "\n\n::GAMES\n" ++ gamesString ++
        "\n\n::HISTORY\n" ++ histString

mainLoop :: [Student] -> [Game] -> History -> IO ()
mainLoop std gms hist = do
    return ()



outputGroupsToFile :: [Group] -> IO()
outputGroupsToFile groups = do 
    let output = intercalate "\n\n" $ map showGroup groups
    writeFile "groups.txt" output

runGenerator :: [Student] -> [Game] -> History -> IO()
runGenerator students games history = do
    let
        newGroups = finalizeGroups (mkStdGen 1) students games history
        output = newFileContents students games (history ++ newGroups)
    putStrLn "Generating groups... this may take a second..."
    sequence_ $ map (putStrLn.(\x->"\n"++showGroup x)) $ newGroups
    outputGroupsToFile newGroups
    putStrLn "\n"
    writeFile "example.txt" output



showStudentInfo :: [Student] -> [Game] -> History -> IO()
showStudentInfo students games hist = do
    let
        instr = instructors students
        showStdFn std =
            let 
                --info about game student made
                game = find (\(_,x,_) -> x == std) games
                isGame = isJust game
                actualGame = fromJust game
                numTimesGamePlayed = length $ filter (\(Group _ x)->x == actualGame) hist
                hasBeenPlayedByInstructor = or $ fmap (/=0) $ fmap (numTimesGamePlayedByStudent actualGame hist) (instr)
                gameText = 
                    if isGame then
                        "\nGame: " ++ show actualGame ++
                        "\n   # times played: " ++ show numTimesGamePlayed ++
                        "\n   played by instructor: " ++ show hasBeenPlayedByInstructor
                    else 
                        "\nNO GAME"

                --info about games student played
                ntgpbstds = map (\x->numTimesGamePlayedByStudent x hist std) games
                strconv ((g,_,_),n) = "   " ++ g ++ " " ++ (show n) 
                ntgpbstdsdisp = intercalate "\n" $ map strconv (zip games ntgpbstds)
            in
            "\nStudent: " ++ std ++
            gameText ++
            "\nPlayed: " ++ (show $ sum ntgpbstds) ++ "\n"
            ++ ntgpbstdsdisp ++
            "\n"
    putStrLn "Showing student info: \n\n"
    sequence_ $ map (putStrLn.showStdFn) students



showGameInfo :: [Student] -> [Game] -> History -> IO()
showGameInfo students games history  = do
    return ()


main = do
    handle <- openFile "example.txt" ReadMode
    contents <- hGetContents handle
    putStrLn $ "Reading from example.txt"
    let
        students = parseStudents contents
        games = parseGames contents
        history = parseHistory contents games
    args <- getArgs
    if args == [] then runGenerator students games history
    else if args !! 0 == "students" then showStudentInfo students games history
    else if args !! 0 == "games" then showGameInfo students games history
    else putStrLn "ERROR, unknown argument"
    hClose handle

    




--IGNORE
--TODO
--if instructor is in group, puts student in the group
adjustGroups :: [Group] -> [Group]
adjustGroups grps =
    let
        groupsWithInstructors =  filter (\(Group x _)-> (length $ filter (\std->isInstructor std) x) > 0) 

    in
        grps --TODO finish
