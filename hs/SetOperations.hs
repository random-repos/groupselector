module SetOperations
(
    partitionUntilEmpty
) where

import Data.List
import System.Random
import Debug.Trace

partitionUntilEmpty :: (Show a) => [a] -> [Int] -> [[a]]
partitionUntilEmpty elts rs =
    --if trace ("partitioning out " ++ (show elts) ++ (show rs)) $ length rs == 0 || head rs > length elts then []
   	if length rs == 0 || head rs > length elts then []
    else [take (head rs) elts] ++ partitionUntilEmpty (drop (head rs) elts) (tail rs)

